migration 1, :initialize do
  up do
    create_table :accounts do
      column :id, Integer, :serial => true
      column :name, DataMapper::Property::String, :length => 255
      column :surname, DataMapper::Property::String, :length => 255
      column :email, DataMapper::Property::String, :length => 255
      column :crypted_password, DataMapper::Property::String, :length => 255
      column :role, DataMapper::Property::String, :length => 255
    end

     #@ Uncomment to support file uploads (using CarrierWave)    
     # create_table :uploads do
     #   column :id, Integer, :serial => true
     #   column :file, DataMapper::Property::Text
     #   column :created_at, DataMapper::Property::DateTime
     # end
        
  end

  down do
    drop_table :accounts
    #@ Uncomment to support file uploads (using CarrierWave)
    # drop_table :uploads
  end
end
