# Seed add you the ability to populate your db.
# We provide you a basic shell for interaction with the end user.
# So try some code like below:
#
#   name = shell.ask("What's your name?")
#   shell.say name
#
# email     = shell.ask "Which email do you want use for logging into admin?"
# password  = shell.ask "Tell me the password to use:"
# shell.say ""

CUR_DIR = File.dirname __FILE__
DATA_DIR = CUR_DIR + "/data"

puts "Auto Upgrading Tables.."
DataMapper.auto_upgrade! #last resort

#Example of using progress bar and seeding
accounts = yaml_load_datafile "accounts.yml"

#How to use the ProgressBar
pbar = ProgressBar.new("Accounts", accounts.count)
accounts.each do |a|
  Account.create(
        :email => a[:email], 
        :name => a[:name], :surname => a[:surname], 
        :password => a[:password], 
        :password_confirmation => a[:password], 
        :role => a[:role])  
  pbar.inc
end
pbar.finish

puts 'done.'
