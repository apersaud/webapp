
def yaml_load_datafile(n)
  file_path = PADRINO_ROOT + "/db/data/"+ n
  YAML::load open(file_path)
end


class String
  def escape_single_quotes
      self.gsub(/[']/, '\\\\\'')
  end
  
  def to_slug
      #strip the string
      ret = self.strip.downcase
      ret.gsub! '.', ''
      #blow away apostrophes
      ret.gsub! /['`;:]/,""

      # @ --> at, and & --> and
      ret.gsub! /\s*@\s*/, " at "
      ret.gsub! /\s*&\s*/, " and "
      ret.strip!
      #replace all non alphanumeric, underscore or periods with underscore
       ret.gsub! /\s*[^A-Za-z0-9\.\-]\s*/, '-'  

       #convert double underscores to single
       ret.gsub! /_+/,"-"
       ret.gsub! /-+/,"-"
       ret.gsub! /^-+/,''
       
       #strip off leading/trailing underscore
       ret.gsub! /\A[_\.]+|[_\.]+\z/,""

       ret
  end
end

def timesince(d)
  distance_of_time_in_words(Time.now, d) +' ago'
end

def http_host
  request.nil? ? 'localhost:3000' : request.env['HTTP_HOST']
end

def site_url(path="/")
    prefix = "http://"+ http_host
    path.start_with?("/") ? prefix + path : "#{prefix}/#{path}"
end