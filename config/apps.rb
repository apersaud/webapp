##
# This file mounts each app in the Padrino project to a specified sub-uri.
# You can mount additional applications using any of these commands below:
#
#   Padrino.mount('blog').to('/blog')
#   Padrino.mount('blog', :app_class => 'BlogApp').to('/blog')
#   Padrino.mount('blog', :app_file =>  'path/to/blog/app.rb').to('/blog')
#
# You can also map apps to a specified host:
#
#   Padrino.mount('Admin').host('admin.example.org')
#   Padrino.mount('WebSite').host(/.*\.?example.org/)
#   Padrino.mount('Foo').to('/foo').host('bar.example.org')
#
# Note 1: Mounted apps (by default) should be placed into the project root at '/app_name'.
# Note 2: If you use the host matching remember to respect the order of the rules.
#
# By default, this file mounts the primary app which was generated with this project.
# However, the mounted app can be modified as needed:
#
#   Padrino.mount('AppName', :app_file => 'path/to/file', :app_class => 'BlogApp').to('/')
#

##
# Setup global project settings for your apps. These settings are inherited by every subapp. You can
# override these settings in the subapps as needed.
#
Padrino.configure_apps do
  # enable :sessions
  #MODIFY THIS VARIABLE - IT SHOULD BE DIFFERENT PER SITE!
  # Run `padrino rake secret` to generate a new secret key
  set :session_secret, '02be17f2140e7d7e31b5ac04c20f56fdc5affecf2ef86bfe130f17a6f030cbcd'
  set :protection, true
  set :protect_from_csrf, true
  
  set :contact_email, ''
  set :google_analytics, "UA-XXXXXXXX-XX"
  
  #See boot file for SMTP_* variables
  #You should use SENDGRID add-on for heroku, otherwise comment this.
  # set :delivery_method, :smtp => { 
  #   :address              => ENV['SMTP_ADDRESS'],
  #   :port                 => 587,
  #   :user_name            => ENV['SMTP_USERNAME'],
  #   :password             => ENV['SMTP_PASSWORD'],
  #   :authentication       => :plain,
  #   :enable_starttls_auto => true  
  # }
  
end

# Mounts the core application for this project
Padrino.mount('Webapp::App', :app_file => Padrino.root('app/app.rb')).to('/')

Padrino.mount("Webapp::Admin", :app_file => File.expand_path('../../admin/app.rb', __FILE__)).to("/admin")