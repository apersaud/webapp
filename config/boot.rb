# Defines our constants
PADRINO_ENV  = ENV['PADRINO_ENV'] ||= ENV['RACK_ENV'] ||= 'development'  unless defined?(PADRINO_ENV)
PADRINO_ROOT = File.expand_path('../..', __FILE__) unless defined?(PADRINO_ROOT)

#If Heroku, use SendGrid add-on (free) otherwise set ENV variables
ENV['SMTP_ADDRESS'] ||= "smtp.sendgrid.net"
ENV['SMTP_USERNAME'] ||= ENV["SENDGRID_USERNAME"]
ENV['SMTP_PASSWORD'] ||= ENV["SENDGRID_PASSWORD"]

#If there is an .env file present with key value pairs of environment variables
#load those before we do anything else. If you don't know about .env file
#check out the gem 'foreman'
override_env = File.dirname(__FILE__)+'/../.env'
if File.exists?(override_env)
  puts "Loading environment overrides (.env)..."
  File.open(override_env).each do |t|
    next if t.start_with?('#')
    next if t.end_with?("=\n")
    (k,v) = t.split "="
    next if v.nil?
    v.chomp!
    next if v.empty?
    ENV[k] = v.chomp
    puts "#{k.upcase}=#{ENV[k]}"
  end
end

# Load our dependencies
require 'rubygems' unless defined?(Gem)
require 'bundler/setup'
Bundler.require(:default, PADRINO_ENV)

##
# ## Enable devel logging
#
# Padrino::Logger::Config[:development][:log_level]  = :devel
# Padrino::Logger::Config[:development][:log_static] = true
#
# ## Configure your I18n
#
I18n.default_locale = :en

#Turns on GZip responses
Padrino.use Rack::Deflater
#
# ## Configure your HTML5 data helpers
#
# Padrino::Helpers::TagHelpers::DATA_ATTRIBUTES.push(:dialog)
# text_field :foo, :dialog => true
# Generates: <input type="text" data-dialog="true" name="foo" />
#
# ## Add helpers to mailer
#
# Mail::Message.class_eval do
#   include Padrino::Helpers::NumberHelpers
#   include Padrino::Helpers::TranslationHelpers
# end

##
# Add your before (RE)load hooks here
#
# If issues with caching add this to before_load

#Encoding.default_internal = nil
#Encoding.default_external = 'UTF-8'

Padrino.before_load do
  #Encoding.default_internal = nil
  #Encoding.default_external = 'UTF-8'
  # APP VERSION
  # UNCOMMENT IF YOU WANT RELEASE VERSION AS ENV IN HEROKU
  ENV["APP_VERSION"] ||= "1"
  if PADRINO_ENV == "production" && (['HEROKU_API_KEY','APP_NAME'].all? { |e| ENV[e] })
    begin
      heroku  = Heroku::API.new
      release = heroku.get_releases(ENV['APP_NAME']).body.last
      ENV["APP_VERSION"] = release["name"].gsub('v','')
    rescue
      puts "!\tUnable to get Heroku Release Version."
      puts "!\tCheck ENV variables APP_NAME and HEROKU_API_KEY"
    end
  end

  puts "----------------------------------------"
  puts "ENVIRONMENT: #{PADRINO_ENV.upcase}"
  puts "VERSION: #{ENV["APP_VERSION"]}"
  puts "DATABASE: #{ENV["DATABASE_URL"]}"
  puts "----------------------------------------"
end

##
# Add your after (RE)load hooks here
#
Padrino.after_load do
  DataMapper.finalize
  if PADRINO_ENV == "production"
     puts "Flushing caches..."
     Webapp::App.cache.flush
     if ['CLOUDFLARE_APIKEY','CLOUDFLARE_EMAIL','CLOUDFLARE_DOMAIN'].all? { |e| ENV[e] }
       puts "Purging CloudFlare Cache..."
       HTTParty.post('https://www.cloudflare.com/api_json.html', 
       {:query => { :a => 'fpurge_ts',  :tkn => ENV['CLOUDFLARE_APIKEY'], :email => ENV['CLOUDFLARE_EMAIL'], :z => ENV['CLOUDFLARE_DOMAIN'], :v => 1}}
       )
     end
   end
end

Padrino.load!
