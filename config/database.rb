##
# A MySQL connection:
# DataMapper.setup(:default, 'mysql://user:password@localhost/the_database_name')
#
# # A Postgres connection:
# DataMapper.setup(:default, 'postgres://user:password@localhost/the_database_name')
#
# # A Sqlite3 connection
# DataMapper.setup(:default, "sqlite3://" + Padrino.root('db', "development.db"))
#

# DataMapper.logger = logger
#makes all string fields in DB VARCHAR(255) instead of 50
DataMapper::Property::String.length(255)
DataMapper::Model.raise_on_save_failure = true

development_db = ENV['DATABASE_URL'] || "sqlite3://" + Padrino.root('db', "database.db")
#DATABASE_URL is the environment variable set in production mode. If not, if we run staging
#which is production mode but locally, we will use the development database.
production_db = ENV['DATABASE_URL'] || development_db

ENV['DATABASE_URL'] = PADRINO_ENV == "production" ? production_db : development_db

case Padrino.env
  when :development then DataMapper.setup(:default, development_db)
  when :production  then DataMapper.setup(:default, production_db)
  when :test        then DataMapper.setup(:default, "sqlite3://" + Padrino.root('db', "test.db"))
end
