
# The template can support handling of uploads using CarrierWave
# We created a model to store urls to the file location
require 'carrierwave/datamapper'
class Upload
  include DataMapper::Resource

  # property <name>, <type>
  property :id, Serial
  property :file, Text, :auto_validation => false

  property :created_at, DateTime
  mount_uploader :file, Uploader
end
