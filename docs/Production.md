Deployment Instructions
===============

The steps below only need to be performed once to setup a production-level web server to run Ruby, Python or Node.js applications.

## Server Requirements ##
This web application is a ruby-based Rack application that runs a server with these requirements:

* Ubuntu Linux or MacOSX 10.5 or later
* [Ruby 1.9.3 or later](http://www.ruby-lang.org/en/)
* [SQLite](http://www.sqlite.org), [MySQL Server](http://dev.mysql.com/downloads/mysql/) or [PostgreSQL](http://www.postgresql.org)
* [Phusion Passenger](https://www.phusionpassenger.com)

## Server Environment Setup ##
To setup an application server to be able to run the web application, follow these steps:

1. Install [Ruby 1.9.3 or later](http://www.ruby-lang.org/en/) on the server.
2. Install [SQLite3](http://www.sqlite.org) on the server. Install any other database servers that this application will use.
3. Install [Phusion Passenger](https://www.phusionpassenger.com) for your current server architecture.

## Site Configuration ##
You will need to modify a few settings to customize the deployment of this application.

1. ** Review the file `config/database.rb` **
	* You can edit the connection to the database in this file. The application will use a local sqlite database file unless a database url is provided as a global environment variable `DATABASE_URL`.
2. ** Review the file `config/apps.rb` **
	* You can modify the settings where indicated with the proper account information where appropriate (ex. Twitter, Google Analytics, Email, etc.).
    * Configure the SMTP information in this file to configure where the server should send emails.

## Production Deployment ##

After unpacking the application bundle and configuring the site options as explained above, you can now prepare the application for deployment by running the following commands below. Note that some of these commands may require sudo priviledges.

	# In the directory where Gemfile is located
	$ gem install rake bundler padrino thin foreman unicorn
	$ bundle install

One the installation of the above gem libraries is successful, you can now seed the database. You can do this by running the following command:

	$ bundle exec padrino rake dm:reset -e production

Once this is done, you should verify that the database has been populated with tables and content. In addition, you can run `bundle exec padrino start -e production` to run a development version of the server to verify that everything is up and running with no problems.

### Heroku-less Deployment ###
If you are not planning on using Heroku for deploying this application, we recommend the newest version of Phusion Passenger. You can use Phusion Passenger in standalone mode to server the web application without the need for Apache or NginX because it comes bundled with the NginX web server core. You can start passenger in standalone mode by running the following command:

    $ gem install passenger
	$ passenger start -p $PORT -e production -d

In the example above, you should replace the `$PORT` variable with the port you'd lke to run the web server on. For ports below 1024, the command may require sudo priviledges. Note that the `-d` option starts the server in the background.

If you already have Apache or NginX installed and would like to use that as your main web server or reverse proxy, we recommend following the directions for configuration on the Phusion Passenger website. Phusion Passenger provides [instructions for other web servers](https://www.phusionpassenger.com/support).

You should now verify that the web application responds with the index page by visiting the configured site on the configured port.

## COPYRIGHT ##
The MIT License (MIT) - Copyright (c) 2013 [Anthony Persaud](http://www.linkedin.com/in/apersaud) - [http://modernistik.com](http://modernistik.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.