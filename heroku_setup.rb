
remove_dir '.git' if File.exists?('.git')
run "heroku login"
begin
  app_name = ask "What will be the heroku app name? : "
  app_name = nil unless yes?("Create ==> #{app_name}.herokuapp.com ? (y/n):")
  result = run "heroku apps:create #{app_name}", :capture => true
  if result.match('git@heroku.com:').nil? 
    puts "!\t#{app_name}.herokuapp.com is not available - try another one"
    app_name = nil
  end
end while app_name.nil?
# 
run "heroku config:set APP_NAME=#{app_name} --app #{app_name}", :capture => true
result = run "heroku addons:add heroku-postgresql:dev --app #{app_name}", :capture => true
if md = result.match('HEROKU_POSTGRESQL_(?<color>\S+)_URL')
  color = md[:color]
  run "heroku pg:promote HEROKU_POSTGRESQL_#{color}_URL --app #{app_name}", :capture => true
end
run "heroku addons:add sendgrid:starter --app #{app_name}", :capture => true
run "heroku addons:add scheduler:standard --app #{app_name}", :capture => true
run "heroku addons:add redistogo:nano --app #{app_name}", :capture => true

insert_into_file "config/boot.rb", "\tENV['APP_NAME'] ||= '#{app_name}'\n", :after => "Padrino.before_load do\n"

#Don't run this setup again.
empty_directory "tmp" unless File.exists?("tmp")
copy_file __FILE__, "tmp/#{File.basename(__FILE__)}"
remove_file __FILE__ if File.exists?(__FILE__)

git :init, "-q"
git :add, "."
git :commit, "-q -m 'initial commit'"
run "heroku git:remote #{app_name} --app #{app_name}", :capture => true
git :push, 'heroku','master'

remove_file '.env' if File.exists?('.env')
create_file ".env", "APP_NAME=#{app_name}\n"

result = run "heroku config", :capture => true
lines = result.split("\n")
lines.shift
lines.each do |l|
  (k,v) = l.split(/:\s*/)
  append_to_file ".env", "##{k}=#{v}\n"
end

run 'heroku open' if yes?("Open #{app_name}.herokuapp.com? (y/n): ")

puts 'Heroku Dev Setup completed.'
