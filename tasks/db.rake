
require 'rake'
require 'progressbar'

EXPORT_DIR = "export/"
directory EXPORT_DIR
namespace :dm do
  desc "Dump data from the current environment's DB."
      task :export => [:environment, EXPORT_DIR] do     
       pbar = ProgressBar.new("Exporting", DataMapper::Model.descendants.entries.size + 1)
     
        DataMapper::Model.descendants.entries.each do |table|
          pbar.inc

          File.open(EXPORT_DIR+"#{table.to_s.downcase}.json", 'w+') { |f| f.print table.all.to_json }
        end #end each
      
        pbar.finish
  end
end