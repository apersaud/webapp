## Padrino Web Application Template ##
This is a [Padrino (Ruby/Rack/Sinatra)](http://www.padrinorb.com) based web application template using [Datamapper](http://datamapper.org) and [Unicorn Web Server](https://github.com/blog/517-unicorn) with easy [Heroku](https://www.heroku.com), SendGrid and [CloudFlare](https://www.cloudflare.com) caching support and integration. This template reduces most of the boilderplate code you'd would write just to get up and running.

### Installation ###
Assuming you have Ruby 1.9.3 or later, the **bundler** gem installed and an optional Heroku account. You can get started easily by running the following commands.

    $ git clone git://github.com/apersaud/webapp.git
    $ cd webapp 
    $ bundle install
    $ padrino g plugin heroku_setup.rb #Optional Heroku support

After you run the commands above, your app should be setup and deployed on Heroku. Please view the `docs` directory for additional documentation.

Note that the Heroku setup plugin will automatically configure your app on Heroku with some of the basic free services (ex. PostgreSQL Dev, REDIS, Scheduler, Sendgrid, etc). You can use the general Heroku commands to upgrade or remove them.

### Heroku Release Version (optional) ###
Another benefit of this template is that we make the release version that has been deployed accessible in the `APP_VERSION` environment variable. To enable this feature, you will need to make sure that you have set the `HEROKU_API_KEY` variable (via `heroku config:set`) with your Heroku API key.

### CloudFlare Cache Purge (optional) ###
This template comes with some minor CloudFlare support. The feature is to provide a way to automatically purge the CloudFlare cache everytime a new application version is deployed. To enable this feature you will need to add 3 new environment variables to Heroku (via `heroku config:set`) and the template will take care of the rest. The variables are:
 
    CLOUDFLARE_APIKEY: your api key
    CLOUDFLARE_EMAIL: should be your cloudflare account email
    CLOUDFLARE_DOMAIN: the domain label as seen in the dashboard

If you don't set all of these variables, the CloudFlare feature is ignored. If you want more information about the CloudFlare API: [http://www.cloudflare.com/docs/client-api.html](http://www.cloudflare.com/docs/client-api.html)

### File Uploads Support ###
CarrierWave is a ruby gem that supports using multipart forms to upload files to the web application. These files can be stored locally or on a remote server like Amazon S3. Take a look at `app/apps.rb` for an example POST handler to capture file contents from a multipart form. The database `Upload` model will store location of the `file` since we have attached that DataMapper property to the Uploader class (see `libs` directory). For more information see [CarrierWave](https://github.com/carrierwaveuploader/carrierwave).

## COPYRIGHT ##
The MIT License (MIT) - Copyright (c) 2013 [Anthony Persaud](http://www.linkedin.com/in/apersaud) - [http://modernistik.com](http://modernistik.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
